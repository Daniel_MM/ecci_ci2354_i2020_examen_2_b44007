package cr.ac.ucr.ecci.eseg.miexamen02.exceptions;

public class CantRetrieveItemsException extends Exception {
    public CantRetrieveItemsException(String msg) {
        super(msg);
    }
}
