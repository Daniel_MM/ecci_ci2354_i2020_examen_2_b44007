package cr.ac.ucr.ecci.eseg.miexamen02.tabletop.interactor;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.CantRetrieveItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.repository.TabletopRepository;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.repository.TabletopRepositoryImpl;

public class TabletopInteractorImpl implements TabletopInteractor {

    private TabletopRepository tabletopRepository;

    OnProgressUpdateListener onProgressUpdateListener;
    OnFinishedListener onFinishedListener;

    public TabletopInteractorImpl() {
        tabletopRepository = TabletopRepositoryImpl.getInstance();
        ((TabletopRepositoryImpl) tabletopRepository).setOnProgressUpdateListener(this);
        ((TabletopRepositoryImpl) tabletopRepository).setOnFinishedListener(this);
    }

    public void setOnProgressUpdateListener(OnProgressUpdateListener onProgressUpdateListener) {
        this.onProgressUpdateListener = onProgressUpdateListener;
    }

    public void setOnFinishedListener(OnFinishedListener onFinishedListener) {
        this.onFinishedListener = onFinishedListener;
    }

    @Override
    public void getItems() {
        try {
            tabletopRepository.obtainItems();
        } catch (CantRetrieveItemsException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(int progress, String message) {
        onProgressUpdateListener.onProgressUpdate(progress, message);
    }

    @Override
    public void onFinished(List<Tabletop> tabletops) {
        onFinishedListener.onFinished(tabletops);
    }
}