package cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.repository;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.CantRetrieveItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.service.ServiceDataSource;

/**
 * Capa de datos.
 * Singleton, selector de origen de los datos.
 */
public interface TabletopRepository
        extends ServiceDataSource.OnProgressUpdateListener, ServiceDataSource.OnFinishedListener {

    interface OnProgressUpdateListener {
        void onProgressUpdate(int progress, String message);
    }

    interface OnFinishedListener {
        void onFinished(List<Tabletop> tabletops);
    }

    void obtainItems() throws CantRetrieveItemsException;
}