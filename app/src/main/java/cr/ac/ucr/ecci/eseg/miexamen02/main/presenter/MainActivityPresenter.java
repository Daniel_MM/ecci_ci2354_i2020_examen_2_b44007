package cr.ac.ucr.ecci.eseg.miexamen02.main.presenter;

import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.interactor.TabletopInteractor;

/**
 * Capa de presentación.
 * Envia solicitudes de actualización al view, y maneja sus eventos.
 */
public interface MainActivityPresenter extends
        TabletopInteractor.OnProgressUpdateListener, TabletopInteractor.OnFinishedListener {

    void onResume();

    void onItemClicked(int position);

    void onDestroy();

    void onBackPressed();
}