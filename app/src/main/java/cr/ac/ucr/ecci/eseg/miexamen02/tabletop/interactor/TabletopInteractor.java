package cr.ac.ucr.ecci.eseg.miexamen02.tabletop.interactor;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.repository.TabletopRepository;

/**
 * Capa del negocio.
 * Contiene los casos de uso del negocio.
 */
public interface TabletopInteractor extends
        TabletopRepository.OnProgressUpdateListener, TabletopRepository.OnFinishedListener {

    interface OnProgressUpdateListener {
        void onProgressUpdate(int value, String message);
    }

    interface OnFinishedListener {
        void onFinished(List<Tabletop> tabletops);
    }

    void getItems();
}