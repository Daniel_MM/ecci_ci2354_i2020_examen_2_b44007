package cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Tabletop implements Parcelable {

    private String identificacion;
    private String nombre;
    private Integer year;
    private String publisher;

    public Tabletop(String identificacion, String nombre, Integer year, String publisher) {
        this.identificacion = identificacion;
        this.nombre = nombre;
        this.year = year;
        this.publisher = publisher;
    }

    protected Tabletop(Parcel in) {
        identificacion = in.readString();
        nombre = in.readString();
        if (in.readByte() == 0) {
            year = null;
        } else {
            year = in.readInt();
        }
        publisher = in.readString();
    }

    public static final Creator<Tabletop> CREATOR = new Creator<Tabletop>() {
        @Override
        public Tabletop createFromParcel(Parcel in) {
            return new Tabletop(in);
        }

        @Override
        public Tabletop[] newArray(int size) {
            return new Tabletop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(identificacion);
        dest.writeString(nombre);
        if (year == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(year);
        }
        dest.writeString(publisher);
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public String getNombre() {
        return nombre;
    }

    public Integer getYear() {
        return year;
    }

    public String getPublisher() {
        return publisher;
    }
}