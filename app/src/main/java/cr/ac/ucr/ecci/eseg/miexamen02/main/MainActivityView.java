package cr.ac.ucr.ecci.eseg.miexamen02.main;

import java.util.ArrayList;
import java.util.HashMap;

import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;

/**
 * Capa de presentación.
 * View.
 */
public interface MainActivityView {

    void constraintUI();

    void showConnectionStarting();

    void showProgress();

    void updateProgress(int progress, String message);

    void hideProgress();

    void setTabletops(ArrayList<HashMap<String, String>> tabletops);

    void showDetails(Tabletop tabletop);

    void hideDetails();
}