package cr.ac.ucr.ecci.eseg.miexamen02.tabletop;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import cr.ac.ucr.ecci.eseg.miexamen02.R;
import cr.ac.ucr.ecci.eseg.miexamen02.main.MainActivity;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;

public class ViewTabletopFragment extends Fragment
        implements View.OnClickListener {

    private static final String TABLETOP = "tabletop";

    private Tabletop tabletop;

    public static ViewTabletopFragment newInstance(Tabletop tabletop) {
        ViewTabletopFragment fragment = new ViewTabletopFragment();
        Bundle args = new Bundle();
        args.putParcelable(TABLETOP, tabletop);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tabletop = getArguments().getParcelable(TABLETOP);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final int resource = R.layout.fragment_view_tabletop_display;
        View view = inflater.inflate(resource, container, false);

        TextView nombre = view.findViewById(R.id.nombre);
        TextView identificacion = view.findViewById(R.id.identificacion);
        TextView publisher = view.findViewById(R.id.publisher);
        TextView year = view.findViewById(R.id.year);

        ImageButton back = view.findViewById(R.id.back);

        nombre.setText(tabletop.getNombre());
        identificacion.setText(tabletop.getIdentificacion());
        publisher.setText(tabletop.getPublisher());
        String yearString = tabletop.getYear().toString();
        year.setText(yearString);

        back.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v){
        ((MainActivity) requireActivity()).hideDetails();
    }
}