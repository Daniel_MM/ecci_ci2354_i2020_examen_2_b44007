package cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model;

import java.util.List;

public class Tabletops {

    private List<Tabletop> miTabletop;

    public Tabletops(List<Tabletop> miTabletop) {
        this.miTabletop = miTabletop;
    }

    public List<Tabletop> getMiTabletop() {
        return miTabletop;
    }
}