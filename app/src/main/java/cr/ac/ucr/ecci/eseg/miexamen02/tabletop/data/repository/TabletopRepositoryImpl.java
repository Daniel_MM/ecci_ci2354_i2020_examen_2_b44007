package cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.repository;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.CantRetrieveItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.service.ServiceDataSource;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.service.ServiceDataSourceImpl;

public class TabletopRepositoryImpl implements TabletopRepository {

    private static volatile TabletopRepository tabletopRepository;

    ServiceDataSource serviceDataSource = new ServiceDataSourceImpl();

    OnProgressUpdateListener onProgressUpdateListener;
    OnFinishedListener onFinishedListener;

    List<Tabletop> tabletops;

    private TabletopRepositoryImpl() {
        ((ServiceDataSourceImpl) serviceDataSource).setOnProgressUpdateListener(this);
        ((ServiceDataSourceImpl) serviceDataSource).setOnFinishedListener(this);
    }

    public static TabletopRepository getInstance() {
        if (tabletopRepository == null) {
            synchronized (TabletopRepositoryImpl.class) {
                if (tabletopRepository == null) {
                    tabletopRepository = new TabletopRepositoryImpl();
                }
            }
        }
        return tabletopRepository;
    }

    public void setOnProgressUpdateListener(OnProgressUpdateListener onProgressUpdateListener) {
        this.onProgressUpdateListener = onProgressUpdateListener;
    }

    public void setOnFinishedListener(OnFinishedListener onFinishedListener) {
        this.onFinishedListener = onFinishedListener;
    }

    @Override
    public void obtainItems() throws CantRetrieveItemsException {
        if (tabletops == null) {
            try {
                ((ServiceDataSourceImpl) serviceDataSource).execute();
            } catch (Exception e) {
                throw new CantRetrieveItemsException(e.getMessage());
            }
        } else {
            onFinished(tabletops);
        }
    }

    @Override
    public void onProgressUpdate(int progress, String message) {
        onProgressUpdateListener.onProgressUpdate(progress, message);
    }

    @Override
    public void onFinished(List<Tabletop> tabletops) {
        this.tabletops = tabletops;
        if (tabletops != null) {
            onFinishedListener.onFinished(tabletops);
        }
    }
}