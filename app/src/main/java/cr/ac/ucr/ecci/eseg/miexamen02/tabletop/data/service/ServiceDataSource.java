package cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.service;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.BaseDataItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;

/**
 * Capa de datos.
 * Servicio web.
 */
public interface ServiceDataSource {

    interface OnProgressUpdateListener {
        void onProgressUpdate(int progress, String message);
    }

    interface OnFinishedListener {
        void onFinished(List<Tabletop> tabletops);
    }

    List<Tabletop> obtainItems() throws BaseDataItemsException;
}