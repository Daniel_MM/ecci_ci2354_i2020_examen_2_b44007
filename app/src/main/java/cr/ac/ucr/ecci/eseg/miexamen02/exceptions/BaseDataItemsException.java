package cr.ac.ucr.ecci.eseg.miexamen02.exceptions;

public class BaseDataItemsException extends Exception {
    public BaseDataItemsException(String msg) {
        super(msg);
    }
}
