package cr.ac.ucr.ecci.eseg.miexamen02.main;

import android.os.Bundle;
import android.os.Handler;
import android.transition.TransitionManager;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.constraintlayout.widget.Group;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.HashMap;

import cr.ac.ucr.ecci.eseg.miexamen02.R;
import cr.ac.ucr.ecci.eseg.miexamen02.main.presenter.MainActicityPresenterImpl;
import cr.ac.ucr.ecci.eseg.miexamen02.main.presenter.MainActivityPresenter;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.LazyAdapter;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.ViewTabletopFragment;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;

public class MainActivity extends AppCompatActivity implements
        MainActivityView, AdapterView.OnItemClickListener {

    private final static int WINDOW_FLG = WindowManager.LayoutParams.FLAG_FULLSCREEN
            | WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION;

    private final static int VISIBILITY = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
            | View.SYSTEM_UI_FLAG_IMMERSIVE;

    private Group principal;
    private ListView listView;

    private Group progress;
    private ProgressBar progressBar;
    private TextView progressStatus;
    private TextView progressMessage;

    private MainActivityPresenter mainActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().setFlags(WINDOW_FLG, WINDOW_FLG);
        super.onCreate(savedInstanceState);
        constraintUI();
        setContentView(R.layout.activity_main);

        principal = findViewById(R.id.principal);
        listView = findViewById(R.id.list);
        listView.setOnItemClickListener(this);

        progress = findViewById(R.id.progress);
        progressBar = findViewById(R.id.progress_bar);
        progressStatus = findViewById(R.id.progress_status);
        progressMessage = findViewById(R.id.progress_message);

        mainActivityPresenter = new MainActicityPresenterImpl(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mainActivityPresenter.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mainActivityPresenter.onDestroy();
    }

    @Override
    public void constraintUI() {
        getWindow().getDecorView().setSystemUiVisibility(VISIBILITY);
    }

    @Override
    public void showConnectionStarting() {
        progressStatus.setText(R.string.conectando);
    }

    @Override
    public void showProgress() {
        progress.setVisibility(View.VISIBLE);
        principal.setVisibility(View.GONE);
    }

    @Override
    public void updateProgress(int progress, String message) {
        progressBar.setProgress(progress);
        String status = progress + "/" + progressBar.getMax();
        progressStatus.setText(status);
        progressMessage.setText(message);
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(View.GONE);
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            principal.setVisibility(View.GONE);
        } else {
            principal.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setTabletops(ArrayList<HashMap<String, String>> tabletops) {
        LazyAdapter adapter = new LazyAdapter(this, tabletops);
        listView.setAdapter(adapter);
    }

    @Override
    public void showDetails(Tabletop tabletop) {
        principal.setVisibility(View.GONE);

        ViewTabletopFragment fragment = ViewTabletopFragment.newInstance(tabletop);

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();

        transaction.addToBackStack(null);
        transaction.replace(R.id.data, fragment);
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        transaction.commit();
    }

    @Override
    public void hideDetails() {
        ConstraintSet constraintSet = new ConstraintSet();

        constraintSet.clone(this, R.layout.fragment_view_tabletop_exit);
        ConstraintLayout constraintLayout = findViewById(R.id.fragment_constraint_layout);

        constraintSet.applyTo(constraintLayout);
        TransitionManager.beginDelayedTransition(constraintLayout);

        FragmentManager manager = getSupportFragmentManager();
        manager.popBackStack();

        new Handler().postDelayed(() -> principal.setVisibility(View.VISIBLE), 300);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        listView.setItemChecked(position, true);
        mainActivityPresenter.onItemClicked(position);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            mainActivityPresenter.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }
}