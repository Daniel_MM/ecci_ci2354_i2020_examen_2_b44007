package cr.ac.ucr.ecci.eseg.miexamen02.main.presenter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.main.MainActivityView;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.LazyAdapter;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.interactor.TabletopInteractor;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.interactor.TabletopInteractorImpl;

public class MainActicityPresenterImpl implements MainActivityPresenter {

    private MainActivityView mainActivityView;
    private TabletopInteractor tabletopInteractor = new TabletopInteractorImpl();
    private List<Tabletop> tabletops;

    public MainActicityPresenterImpl(MainActivityView mainActivityView) {
        this.mainActivityView = mainActivityView;
        ((TabletopInteractorImpl) tabletopInteractor).setOnProgressUpdateListener(this);
        ((TabletopInteractorImpl) tabletopInteractor).setOnFinishedListener(this);
    }

    @Override
    public void onResume() {
        if (mainActivityView != null) {
            if (tabletops == null) {
                mainActivityView.showProgress();
                tabletopInteractor.getItems();
            } else {
                mainActivityView.hideProgress();
            }
            mainActivityView.constraintUI();
        }
    }

    @Override
    public void onItemClicked(int position) {
        if (mainActivityView != null) {
            mainActivityView.showDetails(tabletops.get(position));
        }
    }

    @Override
    public void onDestroy() {
        mainActivityView = null;
    }

    @Override
    public void onBackPressed() {
        mainActivityView.hideDetails();
    }

    @Override
    public void onProgressUpdate(int value, String message) {
        if (value < 0) {
            mainActivityView.showConnectionStarting();
        } else {
            mainActivityView.updateProgress(value, message);
        }
    }

    @Override
    public void onFinished(List<Tabletop> tabletops) {
        this.tabletops = tabletops;
        if (mainActivityView != null) {
            ArrayList<HashMap<String, String>> mapArrayList = new ArrayList<>();
            for (Tabletop tabletop : tabletops) {
                HashMap<String, String> map = new HashMap<>();
                map.put(LazyAdapter.NOMBRE, tabletop.getNombre());
                map.put(LazyAdapter.PUBLISHER, tabletop.getPublisher());
                mapArrayList.add(map);
            }

            mainActivityView.setTabletops(mapArrayList);
            mainActivityView.hideProgress();
        }
    }
}