package cr.ac.ucr.ecci.eseg.miexamen02.tabletop;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import cr.ac.ucr.ecci.eseg.miexamen02.R;

public class LazyAdapter extends BaseAdapter {

    public static final String NOMBRE = "nombre";
    public static final String PUBLISHER = "publisher";

    private ArrayList<HashMap<String, String>> data;
    private Context context;

    public LazyAdapter(Context context, ArrayList<HashMap<String, String>> data) {
        this.data = data;
        this.context = context;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();

            String serviceName = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(serviceName);

            if (inflater != null) {
                convertView = inflater.inflate(R.layout.list_row, parent, false);
                viewHolder.createFromView(convertView);
                convertView.setTag(viewHolder);
            }

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.fillFromItem(data.get(position));

        return convertView;
    }

    private static class ViewHolder {
        private TextView nombre;
        private TextView publisher;

        public void createFromView(View view){
            nombre = view.findViewById(R.id.nombre);
            publisher = view.findViewById(R.id.publisher);
        }

        public void fillFromItem(HashMap<String, String> item){
            nombre.setText(item.get(NOMBRE));
            publisher.setText(item.get(PUBLISHER));
        }
    }
}