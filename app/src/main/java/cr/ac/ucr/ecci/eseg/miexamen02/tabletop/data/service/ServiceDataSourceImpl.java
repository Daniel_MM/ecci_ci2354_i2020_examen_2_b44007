package cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.service;

import android.os.AsyncTask;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.BaseDataItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletops;

public class ServiceDataSourceImpl
        extends AsyncTask<Void, String, List<Tabletop>>
        implements ServiceDataSource {

    private static final String URL = "https://bitbucket.org/lyonv/ci0161_i2020_examenii/raw/996c22731408d9123feac58627318a7859e82367/Tabletop28.json";

    OnProgressUpdateListener onProgressUpdateListener;
    OnFinishedListener onFinishedListener;

    public void setOnProgressUpdateListener(OnProgressUpdateListener onProgressUpdateListener) {
        this.onProgressUpdateListener = onProgressUpdateListener;
    }

    public void setOnFinishedListener(OnFinishedListener onFinishedListener) {
        this.onFinishedListener = onFinishedListener;
    }

    @Override
    public List<Tabletop> obtainItems() throws BaseDataItemsException {
        try {
            publishProgress("-0.01", "");
            URL url = new URL(URL);
            URLConnection urlConnection = url.openConnection();
            int total = urlConnection.getContentLength();



            InputStream mInputStream = (InputStream) urlConnection.getContent();
            InputStreamReader mInputStreamReader = new InputStreamReader(mInputStream);
            BufferedReader responseBuffer = new BufferedReader(mInputStreamReader);

            StringBuilder strBuilder = new StringBuilder();
            String line;

            int actual = -1;
            while ((line = responseBuffer.readLine()) != null) {
                strBuilder.append(line);
                actual += line.length() + 1;
                float progress = (float) actual / total;
                publishProgress(Float.toString(progress), line);
                Thread.sleep((line.length() + 1) * 10);  // Delay para efectos demostrativos
            }

            Gson mJson = new Gson();
            Tabletops tabletops = mJson.fromJson(strBuilder.toString(), Tabletops.class);
            return tabletops.getMiTabletop();
        } catch (Exception e) {
            throw new BaseDataItemsException(e.getMessage());
        }
    }

    @Override
    protected List<Tabletop> doInBackground(Void... voids) {
        try {
            return obtainItems();
        } catch (BaseDataItemsException e) {
            e.printStackTrace();
        }
        publishProgress("0", "Servicio sin conexión");
        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        super.onProgressUpdate(values);
        int progress = Math.round(Float.parseFloat(values[0]) * 100);
        onProgressUpdateListener.onProgressUpdate(progress, values[1]);
    }

    @Override
    protected void onPostExecute(List<Tabletop> result) {
        super.onPostExecute(result);
        onFinishedListener.onFinished(result);
    }
}