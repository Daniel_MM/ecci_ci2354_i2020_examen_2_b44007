package cr.ac.ucr.ecci.eseg.miexamen02;

import android.app.Instrumentation;
import android.content.Context;

import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import cr.ac.ucr.ecci.eseg.miexamen02.exceptions.BaseDataItemsException;
import cr.ac.ucr.ecci.eseg.miexamen02.main.MainActivity;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.model.Tabletop;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.service.ServiceDataSource;
import cr.ac.ucr.ecci.eseg.miexamen02.tabletop.data.service.ServiceDataSourceImpl;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.junit.Assert.assertEquals;


@RunWith(AndroidJUnit4.class)
public class instrumentedTest {

    public static final String NOMBRE = "Catan";
    public static final String IDENTIFICACION = "TN001";
    public static final Integer YEAR = 1995;
    public static final String PUBLISHER = "Kosmos";

    @Rule
    public ActivityTestRule<MainActivity> rule  = new  ActivityTestRule<>(MainActivity.class);

    @Test
    public void testTitle(){
        onView(withId(R.id.titulo)).check(matches(withText("Tabletop Games")));
    }

    @Test
    public void testServcie() throws BaseDataItemsException {
        Context context = InstrumentationRegistry.getInstrumentation().getContext();
        ServiceDataSourceImpl serviceDataSource = new ServiceDataSourceImpl();

        ServiceDataSource.OnProgressUpdateListener onProgressUpdateListener = new ServiceDataSource.OnProgressUpdateListener() {
            @Override
            public void onProgressUpdate(int progress, String message) {
                //do Nothing
            }
        };
        serviceDataSource.setOnProgressUpdateListener(onProgressUpdateListener);
        ServiceDataSource.OnFinishedListener onFinishedListener = new ServiceDataSource.OnFinishedListener() {
            @Override
            public void onFinished(List<Tabletop> tabletops) {
                assertEquals(NOMBRE,tabletops.get(0).getNombre());
                assertEquals(IDENTIFICACION,tabletops.get(0).getIdentificacion());
                assertEquals(YEAR,tabletops.get(0).getYear());
                assertEquals(PUBLISHER,tabletops.get(0).getPublisher());
            }
        };
        serviceDataSource.setOnFinishedListener(onFinishedListener);
    }
}